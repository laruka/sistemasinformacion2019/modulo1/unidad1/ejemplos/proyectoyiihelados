<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "helados".
 *
 * @property int $id
 * @property string $nombre
 * @property string $sabor
 * @property int $tamaño
 */
class Helados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tamaño'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['sabor'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Referencia',  /*podemos poner el nombre que queramos aqui*/ /*nombre para mostrar*/
            'nombre' => 'Nombre',
            'sabor' => 'Sabor',
            'tamaño' => 'Tamaño 1,2,3,4',
        ];
    }
}
